
#include "RDS.h"

struct RDSDevInfo RDSInfo;

int main (void)
{

    InitRDSDev();
    RDSTask();

    return 0;
}

void InitRDSDev(void)
{
    RDSDev.flag = &RDSFlag;
    RDSDev.BlockData = &rdsBlockData;
    RDSDev.info = &RDSInfo;
}

void RDSTask(void)
{
    while (1)
    {
        OSTimeDly(10);

        if (!IsRDSEnable())
        {
            continue;
        }

        if (IsRDSHaveMsg())
        {
            ProcRdsMsg();
        }
        ScanRdsMsg();


    }
}

void ProcRdsMsg(void)
{
    uchar *BlockDataCount = RDSDev.BlockDataCount;

    struct RDSDevInfo *info = &RDSDev.info;
    ushort *PI = &info->PI;
    uchar *PTY = &RDSDev.info.PTY;
    uchar *TP = &RDSDev.info.A_or_B;
    uchar *TA = &RDSDev.info.TP;


    struct RDSData* data;
    uchar Group;
    enum {
        Version_A,
        Version_B
    } Version;



    while (*BlockDataCount)
    {

        data = (struct RDSData *) &rdsBlockData[*BlockDataCount];

        *PI = data->PI;

        if (data->Sign.B0 == 0)
        {
            Version = Version_A;
        }else
        {
            Version = Version_B;
        }

        Group = data->Sign.A0_to_A3;

        *PTY = data->Sign.PTY;
        if (Group != 2)
        {
            *TA = data->Sign.TA;
            *TP = data->Sign.A_or_B;
        }

        switch (Group)
        {
            case 0:
                info->type = Group_0;
                info->msg.g0.AF0 = data->msg0 & 0x00FF;
                info->msg.g0.AF1 = (data->msg0 & 0xFF00)>>8;
            break;
            case 2:
            break;
            case 4:
            break;
            default:
                info->type = GroupOther;
            break;
        }



        SetFlagHaveDevInfo();
        *BlockDataCount--;
    }


    return;
}

void ScanRdsMsg(void)
{
    uint RDSRecvData;

    if (!IsRDSITEnable())

    {
        return;
    }
    /*loop decode rds msg*/
    while (1)
    {
        if (getRdsITCount() == 0)
        {
            printf("don't have msg IT\r\n");
            return;
        }else
        {
            RDSDev.ITCount--;
        }
        RDSRecvData = RDSRecv();
        DecodeRdsMsg(RDSRecvData);
    }

}

void DecodeRdsMsg(uint RDSRecvData)
{
    #define Block_A 0
    #define Block_B 1
    #define Block_C 2
    #define Block_D 3


    ushort  *blockA = &(RDSDev.BlockData->blockA),
            *blockB = &(RDSDev.BlockData->blockB),
            *blockC = &(RDSDev.BlockData->blockC),
            *blockD = &(RDSDev.BlockData->blockD);

    uchar block;
    static enum {
        GET_NO_BLOCK,
        GET_BLOCK_1,
        GET_BLOCK_2,
        GET_BLOCK_3,
        GET_BLOCK_4
    }DecodeState = GET_NO_BLOCK;

    RDSFrame.frame = RDSRecvData;
    /*cheak if the data valid*/
    if (RDSFrame.Z==1 && RDSFrame.DOK==1 && RDSFrame.SYN==1 && RDSFrame.BM==0
     && RDSFrame.SYNDROME0==0 && RDSFrame.SYNDROME1==0 && RDSFrame.SYNDROME2==0
     && RDSFrame.SYNDROME3==0 && RDSFrame.SYNDROME4==0)
    {
        //RDSDev.ValidDataCount++;
    }
    else
    {
        return;
    }

    block = RDSFrame.BLOCK_TYPE0 + (RDSFrame.BLOCK_TYPE0*2);
    if (block < 0 || block > 3)
    {
        printf("block err");
        return;
    }


    if (DecodeState == GET_NO_BLOCK)
    {
        RDSFlag.GetBlock1 = 0;
        RDSFlag.GetBlock2 = 0;
        RDSFlag.GetBlock3 = 0;
        RDSFlag.GetBlock4 = 0;
        memset(blockA,0,sizeof(ushort));
        memset(blockB,0,sizeof(ushort));
        memset(blockC,0,sizeof(ushort));
        memset(blockD,0,sizeof(ushort));
    }

    switch (block)
    {
        case Block_A:
            {
                RDSFlag.GetBlock1 = 1;
                DecodeState = GET_BLOCK_1;
                RDSFlag.GetBlock2 = 0;
                *blockA = RDSRecvData;
            }
        break;
        case Block_B:
            if (DecodeState == GET_BLOCK_1)
            {
                RDSFlag.GetBlock2 = 1;
                DecodeState = GET_BLOCK_2;
                RDSFlag.GetBlock3 = 0;
                *blockB= RDSRecvData;
            }else
            {
                DecodeState = GET_NO_BLOCK;
            }
        break;
        case Block_C:
            if (DecodeState == GET_BLOCK_2)
            {
                RDSFlag.GetBlock2 = 1;
                DecodeState = GET_BLOCK_3;
                RDSFlag.GetBlock4 = 0;
                *blockC= RDSRecvData;
            }else
            {
                DecodeState = GET_NO_BLOCK;
            }
        break;
        case Block_D:
            if (DecodeState == GET_BLOCK_3)
            {
                RDSFlag.GetBlock2 = 1;
                DecodeState = GET_BLOCK_4;
                *blockD = RDSRecvData;
            }else
            {
                DecodeState = GET_NO_BLOCK;
            }
        break;
    }

    if (DecodeState == GET_BLOCK_4)
    {
        RDSFlag.HaveMsg = 1;
        RDSDev.BlockDataCount++;
    }


    return;
}


uint RDSRecv(void)
{
    return 0;
}



bool IsRDSEnable(void)
{
    return RDSFlag.RDSEnable;
}

bool IsRDSHaveMsg(void)
{
    return RDSFlag.HaveMsg;
}

bool IsRDSITEnable(void)
{
    return RDSFlag.ITEnable;
}

bool IsRDSHaveDevInfo(void)
{
    return RDSFlag.HaveDevInfo;
}

uchar getRdsITCount(void)
{
    return RDSDev.ITCount;
}

void SetFlagHaveDevInfo(void)
{
    RDSFlag.HaveDevInfo = 1;
}

bool CheckRDSDevInfo(void)
{
    if (RDSFlag.HaveDevInfo == 1)
    {
        RDSFlag.HaveDevInfo = 0;
        return 1;
    }
    return 0;
}

