#ifndef RDSAPP_H_INCLUDED
#define RDSAPP_H_INCLUDED

#include "RDSInterface.h"

struct SETUP{
        uchar AFMode : 1;
        uchar TPMode : 1;
        uchar TAMode : 1;
        uchar reserveB3 : 1;
        uchar reserveB4 : 1;
        uchar reserveB5 : 1;
        uchar reserveB6 : 1;
        uchar reserveB7 : 1;
        uchar PTY;
}rdsSetup;

#define AFLISTSIZE 5

struct AFLIST{
    ushort PI;
    uchar Offset;
    uchar Freq[AFLISTSIZE];
}AFList;




#endif //RDSAPP_H_INCLUDED