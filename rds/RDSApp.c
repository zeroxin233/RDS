
#include "RDSApp.h"

void RDSMain(void)
{
    struct SETUP *setup = &rdsSetup;
    struct RDSDevInfo *info;
    enum TYPE type;
    
    static ushort NowPI;
    while (1)
    {
        OSTimeDly(10);
        


        if (setup->TPMode)
        {
            if (info->TP)
            {
                SendTPMsg();
            }
        }

        if (setup->TAMode)
        {
            if (info->TA)
            {
                SendTAMsg();
            }               
        }

        
        



        if (CheckRDSDevInfo())
        {
            info = &RDSInfo;

            if (setup->PTY)
            {
                if (setup->PTY != info->PTY)
                    continue;
            }
            
            if (NowPI != info->PI)
            {
                NowPI = info->PI;
                ProcPIChange(NowPI);
            }

            type = RDSInfo->type;

            switch (type)
            {
                case Group_0:
                    
                    ProcGroup0(&(info->msg));
                    if (setup->AFMode)
                    {
                        SendAFMsg();
                    }
                     
                break;
                case Group_2:
                break;
                case Group_4:
                break;
               
            }
            
        }
     }

    
}

void ProcPIChange(ushort PI)
{
    struct AFLIST *list = &AFList;
    list->PI = PI;
    list->Offset = 0;
    memset(&(list->Freq[]), 0, sizeof(uchar));
}

void ProcGroup0(union Msg* msg)
{
    uchar AFFreq;
    AFFreq = msg.g0.AF0;
    if (AFFreq<205 && AFFreq>0)
    {
        PushAFFreqToList();
    }

    AFFreq = msg.g0.AF1;
    if (AFFreq<205 && AFFreq>0)
    {
        PushAFFreqToList();
    }

    
}

void PushAFFreqToList(uchar freq)
{
    struct AFLIST *list = &AFList;
    int listNum;

    if (list->Offset > AFLISTSIZE)
    {
        printf("FULL !");
        return;
    }
    
    for (listNum=0; listNum < list->Offset; listNum++)
    {
        if (freq == list->Freq[listNum])
        {
            return;
        }
    }
    
    list->Freq[list->Offset] = freq;
    list->Offset++;
    
}

void SendTPMsg(void)
{

}

void SendTAMsg(void)
{

}

void SendAFMsg(void)
{

}

void SetAFFreq(void)
{

}






