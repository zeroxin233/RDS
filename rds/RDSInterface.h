#ifndef RDSINTERFACE_H_INCLUDED
#define RDSINTERFACE_H_INCLUDED




enum TYPE{
        Group_0,
        Group_2,
        Group_4,
        GroupOther
};

union Msg{
        struct Group_0{
            uchar AF_0;
            uchar AF_1;
        }g0;
        struct Group_2{
            ushort Text0;
            ushort Text1;
            uchar  offset;
        }g2;
        struct Group_4{
            int day;
            uchar hour;
            uchar min;
        }g4;
};

struct RDSDevInfo{
    ushort PI;
    uchar PTY;
    uchar TP;
    uchar TA;
    
    enum TYPE type;

    union Msg msg;
};

extern struct RDSDevInfo RDSInfo;


bool CheckRDSDevInfo(void)


#endif //RDSINTERFACE_H_INCLUDED