#ifndef RDS_H_INCLUDED
#define RDS_H_INCLUDED

#include <stdio.h>
#include "other.h"
#include "RDSInterface.h"

/*data config*/
#define MAXBLOCKDATA 10

const struct {
   uchar MaxITCount;
   uchar MaxBlockData;
}RDSDevConfigTable = {
    .MaxITCount = 200,
    .MaxBlockData = MAXBLOCKDATA
};



union RDSFLAG{
    uchar flag;
    struct{
        uchar RDSEnable : 1;
        uchar ITEnable : 1;
        uchar GetBlock1 : 1;
        uchar GetBlock2 : 1;
        uchar GetBlock3 : 1;
        uchar GetBlock4 : 1;
        uchar HaveMsg : 1;
        uchar HaveDevInfo : 1;
    }
} RDSFlag;

struct RDSBlockData{
    ushort blockA;
    ushort blockB;
    ushort blockC;
    ushort blockD;
}rdsBlockData[MAXBLOCKDATA];



struct RDSDEV{
    uint ITCount;
    uint ValidDataCount;
    uchar BlockDataCount;
    /*dev state flag*/
    union RDSFLAG *flag;
    /*dev recv block data*/
    struct RDSBlockData* BlockData;
    /*dev info*/
    struct RDSDevInfo* info;
}RDSDev;

union {
    uint frame;
    struct{
        ushort BLOCKDATA;
        uchar QUALCOUNT0 : 1;
        uchar QUALCOUNT1 : 1;
        uchar QUALCOUNT2 : 1;
        uchar QUALCOUNT3 : 1;
        uchar BLOCK_TYPE0 : 1;
        uchar BLOCK_TYPE1 : 1;
        uchar BE : 1;
        uchar Z : 1;

        uchar SYN : 1;
        uchar DOK : 1;
        uchar BM : 1;
        uchar SYNDROME0 : 1;
        uchar SYNDROME1 : 1;
        uchar SYNDROME2 : 1;
        uchar SYNDROME3 : 1;
        uchar SYNDROME4 : 1;
   }
}RDSFrame;

struct RDSData{
    ushort PI;
    struct {
        ushort address : 4;
        ushort A_or_B : 1;
        ushort PTY : 5;
        ushort TP : 1;
        ushort B0 : 1;
        ushort A0_to_A3 : 4;
    }Sign;
    ushort msg0;
    ushort msg1;

};

void InitRDS(void);
void RDSTask(void);
void DecodeRdsMsg(uint RDSRecvData);
bool IsRDSEnable(void);
bool IsRDSHaveMsg(void);
bool IsRDSITEnable(void);
bool IsRDSHaveDevInfo(void);
uchar getRdsITCount(void);
uint RDSRecv(void);
void SetFlagHaveDevInfo(void);

#endif // RDS_H_INCLUDED
