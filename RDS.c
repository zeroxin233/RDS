typedef uchar unsigned char 
typedef uint unsigned int

const struct {
   uchar MaxITCount;
   uchar MaxBlockData;
}RDSDevConfigTable = {
    .MaxITCount = 200,
    .MaxBlockData = 10
};


union RDSFLAG{
    uchar flag;
    struct{
        uchar RDSEnable : 1;
        uchar ITEnable : 1;
        uchar GetBlock1 : 1;
        uchar GetBlock2 : 1;
        uchar GetBlock3 : 1;
        uchar GetBlock4 : 1;
        uchar HaveMsg : 1;
    }
}RDSFlag;

struct RDSBlockData{
    u16 blockA;
    u16 blockB;
    u16 blockC;
    u16 blockD;
}rdsBlockData[RDSDevConfigTable.MaxBlockData];

struct SETUP{
        uchar PTYMode;
        uchar AFMode : 1;
        uchar TPMode : 1;
        uchar TAMode : 1;
        uchar reserveB3 : 1;
        uchar reserveB4 : 1;
        uchar reserveB5 : 1;
        uchar reserveB6 : 1;
        uchar reserveB7 : 1;
}rdssetup;

struct RDSData{
    u16 PI;
    union{
        u16 sign;
        struct {
            u16 address : 4;
            u16 A_or_B : 1;
            u16 PTY : 5;
            u16 TP : 1;
            u16 B0 : 1;
            u16 A0_to_A3 : 4;
        }
    }
    u16 msg0;
    u16 msg1;
    
};

struct {
    uint ITCount;
    uint ValidDataCount;
    uchar BlockDataCount;
    /*user set para*/
    (struct SETUP)* setup;
    /*dev state flag*/
    (union RDSFLAG)* flag;
    /*dev recv block data*/
    (struct RDSBlockData)* BlockData;
    /*dev decoded data*/
    //(struct RDSData)* data;
}RDSDev;




uint getRdsITCount(void)
{
    return RDSDev.ITCount;
}

void INCRDSITCount(void)
{
    if ( (RDSDev.ITCount++) >= RDSDevConfigTable.MaxITCount)
    {
        RDSDev.ITCount = RDSDevConfigTable.MaxITCount;
    }
    
}




void delay(int ms);
void scanRdsMsg(void);
void procRdsMsg(void);



void rdsTask(void)
{
    
    while (1)
    {
        /*task loop time*/
        delay(10);

        if (RDSFlag.RDSEnable == 0)
        {
            continue;
        }
        
        /*task main fun*/
        if (RDSFlag.HaveMsg)
        {
            procRdsMsg();
        }
        scanRdsMsg();

        

        
    }
    
}

void rdsTaskinit(void)
{
    RDSDev.BlockData = &rdsBlockData[];
    RDSDev.setup = &rdssetup;
}


void scanRdsMsg(void)
{
    uint RDSRecvData;
    
    if (RDSFlag.ITEnable == 0)
    {   
        return;
    }
    /*loop decode rds msg*/
    while (1)
    {
        if (getRdsITCount() == 0)
        {
            Print("don't have msg IT\r\n");
            return;
        }else
        {
            RDSDev.ITCount--;
        }
        RDSRecvData = RDSRecv();
        decodeRdsMsg(RDSRecvData);
    }
   
}

bool getUintBit(uint Uintdata,uchar bit)
{
    return (bool)( (Uintdata>>bit)&(0x00000001) );
}

union {
    uint frame;
    struct{
        u16 BLOCKDATA;
        uchar QUALCOUNT0 : 1;
        uchar QUALCOUNT1 : 1;
        uchar QUALCOUNT2 : 1;
        uchar QUALCOUNT3 : 1;
        uchar BLOCK_TYPE0 : 1;
        uchar BLOCK_TYPE1 : 1;
        uchar BE : 1;
        uchar Z : 1;

        uchar SYN : 1;
        uchar DOK : 1;
        uchar BM : 1;
        uchar SYNDROME0 : 1;
        uchar SYNDROME1 : 1;
        uchar SYNDROME2 : 1;
        uchar SYNDROME3 : 1;
        uchar SYNDROME4 : 1;
   }
}RDSFrame;


void decodeRdsMsg(uint RDSRecvData)
{
    
   

    u16  *blockA = &(RDSDev.BlockData->blockA),
         *blockB = &(RDSDev.BlockData->blockB),
         *blockC = &(RDSDev.BlockData->blockC),
         *blockD = &(RDSDev.BlockData->blockD);

    uchar block;
    static enum {
        GET_NO_BLOCK,
        GET_BLOCK_1,
        GET_BLOCK_2,
        GET_BLOCK_3,
        GET_BLOCK_4
    }DecodeState = GET_NO_BLOCK;
    
    RDSFrame.frame = RDSRecvData;
    /*cheak if the data valid*/
    if (RDSFrame.Z==1 && RDSFrame.DOK==1 && RDSFrame.SYN==1 && RDSFrame.BM==0 
     && RDSFrame.SYNDROME0==0 && RDSFrame.SYNDROME1==0 && RDSFrame.SYNDROME2==0 
     && RDSFrame.SYNDROME3==0 && RDSFrame.SYNDROME4==0)
    {
        //RDSDev.ValidDataCount++;
    }
    else
    {
        return;
    }

    block = RDSFrame.BLOCK_TYPE0 + (RDSFrame.BLOCK_TYPE0*2);
    if (block < 0 || block > 3)
    {
        Printf("block err");
        return;
    }

    
    if (DecodeState == GET_NO_BLOCK)
    {
        RDSFlag.GetBlock1 = 0;
        RDSFlag.GetBlock2 = 0;
        RDSFlag.GetBlock3 = 0;
        RDSFlag.GetBlock4 = 0;
        memset(blockA,0,sizeof(u16));
        memset(blockB,0,sizeof(u16));
        memset(blockC,0,sizeof(u16));
        memset(blockD,0,sizeof(u16));
    }

    switch (block)
    {
        case 0:
            {
                RDSFlag.GetBlock1 = 1;
                DecodeState = GET_BLOCK_1;
                RDSFlag.GetBlock2 = 0;
                *blockA = RDSRecvData;
            }
        break;
        case 1:
            if (DecodeState == GET_BLOCK_1)
            {
                RDSFlag.GetBlock2 = 1;
                DecodeState = GET_BLOCK_2;
                RDSFlag.GetBlock3 = 0;
                *blockB= RDSRecvData;
            }else
            {
                DecodeState = GET_NO_BLOCK;
            }
        break;
        case 2:
            if (DecodeState == GET_BLOCK_2)
            {
                RDSFlag.GetBlock2 = 1;
                DecodeState = GET_BLOCK_3;
                RDSFlag.GetBlock4 = 0;
                *blockC= RDSRecvData;
            }else
            {
                DecodeState = GET_NO_BLOCK;
            }
        break;
        case 3:
            if (DecodeState == GET_BLOCK_3)
            {
                RDSFlag.GetBlock2 = 1;
                DecodeState = GET_BLOCK_4;
                *blockD = RDSRecvData;
            }else
            {
                DecodeState = GET_NO_BLOCK;
            }
        break;
    }

    if (DecodeState == GET_BLOCK_4)
    {
        RDSFlag.HaveMsg = 1;
        RDSDev.BlockDataCount++;
    }
    
    
    return;
}


void procRdsMsg(void)
{
    (struct SETUP) *SetUp = RDSDev.setup;
    uchar *BlockDataCount = RDSDev.BlockDataCount;
    
    while (*BlockDataCount)
    {
        (struct RDSData) data = (struct RDSData)rdsBlockData[*BlockDataCount]

        if (data.B0)
        {

        }else
        {
    
        }

        
        *BlockDataCount--;
    }

    
    return;
}


